These benchmarks have now been incorporated into the main url-builder project and are no longer maintained.

This benchmarks [url-builder](https://github.com/palominolabs/url-builder)'s percent encoding and decoding routines against the ones available in Java's URLEncoder and URLDecoder classes. The two approaches don't produce exactly the same results (URLEncoder and URLDecoder actually do HTML form encoding, which follows slightly different rules), so it's not a 100% fair comparison.

Run the benchmarks:
```
mvn clean package && java -jar target/microbenchmarks.jar -f 10 -i 20 -wi 10
```

As of 1.1.0-SNAPSHOT, `url-builder`'s performance is significantly better for small strings and a little better for large strings:

```
Benchmark                                                  Mode Thr     Count  Sec         Mean   Mean error    Units
c.p.h.u.PercentEncoderBenchmark.testPercentDecodeLarge    thrpt   1        30    1        1.227        0.009   ops/ms
c.p.h.u.PercentEncoderBenchmark.testPercentDecodeSmall    thrpt   1        30    1     1131.598       22.834   ops/ms
c.p.h.u.PercentEncoderBenchmark.testPercentEncodeLarge    thrpt   1        30    1        2.108        0.050   ops/ms
c.p.h.u.PercentEncoderBenchmark.testPercentEncodeSmall    thrpt   1        30    1     1901.091       51.345   ops/ms
c.p.h.u.PercentEncoderBenchmark.testUrlDecodeLarge        thrpt   1        30    1        1.048        0.016   ops/ms
c.p.h.u.PercentEncoderBenchmark.testUrlDecodeSmall        thrpt   1        30    1     1006.702       12.587   ops/ms
c.p.h.u.PercentEncoderBenchmark.testUrlEncodeLarge        thrpt   1        30    1        1.163        0.005   ops/ms
c.p.h.u.PercentEncoderBenchmark.testUrlEncodeSmall        thrpt   1        30    1     1160.900       25.255   ops/ms
```
