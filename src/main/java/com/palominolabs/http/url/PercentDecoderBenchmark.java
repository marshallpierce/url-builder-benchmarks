/*
 * Copyright (c) 2005, 2013, Oracle and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation.  Oracle designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Oracle in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Oracle, 500 Oracle Parkway, Redwood Shores, CA 94065 USA
 * or visit www.oracle.com if you need additional information or have any
 * questions.
 */

package com.palominolabs.http.url;

import com.google.common.base.Throwables;
import org.openjdk.jmh.annotations.GenerateMicroBenchmark;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;

import java.nio.charset.CharacterCodingException;
import java.nio.charset.StandardCharsets;

import static com.palominolabs.http.url.PercentEncoderBenchmark.LARGE_STRING_MIX;
import static com.palominolabs.http.url.PercentEncoderBenchmark.SMALL_STRING_MIX;

public class PercentDecoderBenchmark {

    static final String SMALL_STRING_ENCODED;
    static final String LARGE_STRING_ENCODED;

    static {
        PercentEncoder encoder = UrlPercentEncoders.getQueryEncoder();
        try {
            SMALL_STRING_ENCODED = encoder.encode(SMALL_STRING_MIX);
        } catch (CharacterCodingException e) {
            throw Throwables.propagate(e);
        }
        try {
            LARGE_STRING_ENCODED = encoder.encode(LARGE_STRING_MIX);
        } catch (CharacterCodingException e) {
            throw Throwables.propagate(e);
        }
    }

    @State(Scope.Thread)
    public static class ThreadState {
        PercentDecoder decoder = new PercentDecoder(StandardCharsets.UTF_8.newDecoder());
    }

    @GenerateMicroBenchmark
    public String testPercentDecodeSmall(ThreadState state) throws CharacterCodingException {
        return state.decoder.decode(SMALL_STRING_ENCODED);
    }

    @GenerateMicroBenchmark
    public String testPercentDecodeLarge(ThreadState state) throws CharacterCodingException {
        return state.decoder.decode(LARGE_STRING_ENCODED);
    }
}
